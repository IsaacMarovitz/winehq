<!--TITLE:[Get Involved]-->

<h1 class="title">Get Involved</h1>

<p>The Wine Project is largely a volunteer project and we welcome
and value volunteers of all kinds.  This page has links to pages describing how to get more involved with Wine.</p>

<div class="black inverse bold cornerround padding-sm">
    <div class="row">
        <div class="col-xs-3 col-md-2">Link</div>
        <div class="col-xs-9 col-md-10">Description</div>
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="https://wiki.winehq.org/Main_Page#Contribute">Contributing to Wine</a>
    </div>
    <div class="col-xs-9 col-md-10">
        A list of ways to contribute to Wine, with further links.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="https://wiki.winehq.org/Developers">Developer Documentation</a>
    </div>
    <div class="col-xs-9 col-md-10">
        The developer pages provide a great deal of information about how
        to work with the Wine source code and how to contribute code to Wine.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="{$root}/forums">Forums and Mailing Lists</a>
    </div>
    <div class="col-xs-9 col-md-10">
        Most user support happens on our forums, and most technical
        Wine discussion takes place on the wine-devel mailing list.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="{$root}/irc">IRC</a>
    </div>
    <div class="col-xs-9 col-md-10">
        Our IRC chat rooms can be a good place for more informal conversations.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="https://wiki.winehq.org/WineConf">WineConf</a>
    </div>
    <div class="col-xs-9 col-md-10">
        We hold a conference annually. Everyone is welcome, and coming to
        our conference is a great way to get involved.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="https://wiki.winehq.org/Summer_Of_Code">Summer of Code</a>
    </div>
    <div class="col-xs-9 col-md-10">
        Wine is a regular participant in the Google Summer of Code program.
        Students can be paid to help improve Wine!
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-3 col-md-2">
        <a href="https://www.codeweavers.com/about/jobs">Jobs at CodeWeavers</a>
    </div>
    <div class="col-xs-9 col-md-10">
        CodeWeavers employs a range of staff to develop Wine.  If you think
        working on Free Software full time would be rewarding,  you might consider
        applying for a job with CodeWeavers.
    </div>
</div>
