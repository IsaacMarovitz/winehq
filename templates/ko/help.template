<!--TITLE:[도움 받기]-->

<h1 class="title">도움 받기</h1>

<p>Wine 사용 중 도움을 받을 수 있는 여러 종류의 방법을 소개합니다.</p>

<div class="black inverse bold cornerround padding-sm">
    <div class="row">
        <div class="col-xs-2 col-md-1">옵션</div>
        <div class="col-xs-10 col-md-11">옵션에 대한 설명</div>
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="http://www.codeweavers.com"><i class="fas fa-money-bill-alt fa-2x"></i><br>상용</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="http://www.codeweavers.com">CodeWeavers</a>는 Wine의 상용 지원을 제공합니다.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/FAQ"><i class="fas fa-info-circle fa-2x"></i><br>FAQ</a>
    </div>
    <div class="col-xs-10 col-md-11">
        자주 묻는 질문과 답변을
        <a href="https://wiki.winehq.org/FAQ">FAQ</a> 페이지에 정리해 두었습니다.
        문제에 대한 해결책을 여기에서 먼저 찾아 보십시오.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/documentation"><i class="fas fa-folder-open fa-2x"></i><br>문서</a>
    </div>
    <div class="col-xs-10 col-md-11">
        질문에 대한 답을 얻기 위해서 온라인 <a href="{$root}/documentation">문서</a>를 읽어 보는 것을 추천합니다.
        Wine을 처음으로 사용한다면
        <a href="https://wiki.winehq.org/Wine_Installation_and_Configuration">Wine 설치와 설정</a> 위키 페이지에도 처음 사용자에게 도움이 되는 정보가 있습니다.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/"><i class="fas fa-book fa-2x"></i><br>위키</a>
    </div>
    <div class="col-xs-10 col-md-11">
        질문에 대한 답을 <a href="https://wiki.winehq.org/">위키</a>에서도 찾아볼 수 있습니다.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//forums.winehq.org/"><i class="fas fa-comments fa-2x"></i><br>포럼</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="//forums.winehq.org/">웹</a>/<a href="{$root}/forums">이메일</a> 기반 Wine 사용자 커뮤니티가 있습니다.
        Wine 사용자들이 질문을 올릴 수 있고 문제의 해결책을 찾을 수 있습니다.
        해당 커뮤니티의 기반이 되는 <a href="{$root}/forums">메일링 리스트</a>를 찾아 보는 것도 추천합니다.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/irc"><i class="fas fa-comment-alt fa-2x"></i><br>IRC</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="{$root}/irc">IRC 채널</a>을 통해서 Wine 커뮤니티 실시간 채팅에 참여할 수 있습니다.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//appdb.winehq.org/"><i class="fas fa-database fa-2x"></i><br>AppDB</a>
    </div>
    <div class="col-xs-10 col-md-11">
        개별 프로그램 사용에 대한 도움을 받으려면 <a href="//appdb.winehq.org/">프로그램 데이터베이스</a>를
        둘러보는 것을 추천합니다.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//bugs.winehq.org/"><i class="fas fa-bug fa-2x"></i><br>Bugzilla</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="//bugs.winehq.org/">Bugzilla 버그 추적기</a>에는 과거에 제기되었거나 현재 작업 중인 문제점에 대한 정보가 있습니다.
    </div>
</div>

<p>웹 사이트에 문제가 있거나 오탈자를 발견하셨나요? <a href="mailto:web-admin@winehq.org">웹 팀</a>에 연락해 주십시오.</p>
