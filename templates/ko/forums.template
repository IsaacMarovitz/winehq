<!--TITLE:[메일링 리스트/포럼]-->

<h1 class="title">주요 메일링 리스트 및 포럼</h1>

<p>Wine에는 여러 메일링 리스트와 포럼이 있습니다.
다음은 사용자에게 유용한 메일링 리스트와 포럼 목록입니다.</p>

<ul>
    <li><a href="//forum.winehq.org">WineHQ 포럼</a></li>

    <li>우분투 사용자라면 <a href="http://ubuntuforums.org/forumdisplay.php?f=313">우분투 포럼의 Wine 게시판</a>을 확인해 보는 것도 추천합니다.

    <li>새 릴리스 알림을 받으려면 <a href="//www.winehq.org/mailman/listinfo/wine-announce">Wine-Announce 메일링 리스트</a>를 구독하십시오.</li>
</ul>

<p>인기 있는 배포판의 주 포럼에 Wine 게시판을 만드는 것 외에
다른 Wine 영어 포럼을 만드는 것은 추천하지 않습니다.</p>

<p>(특히 비영어권의) 다른 활성화된 Wine 포럼을 알고 있다면
저희에게 연락해 주시면 목록에 추가하겠습니다.
(dank@kegel.com 주소로 이메일을 보내 주십시오.)</p>

<h2>모든 WineHQ 메일링 리스트</h2>

<p>WineHQ에는 패치 제출, Git 커밋 추적, Wine 토론에 대한 메일링 리스트가
있습니다. 각각 메일링 리스트의 구독 옵션을 설정할 수 있으며, <a
href="https://www.winehq.org/mailman3/postorius/lists/?all-lists">웹 페이지</a>.</p>

<p><b>메모:</b> 메일링 리스트에 글을 올리기 전에 구독하고 있어야 합니다.
구독하지 않은 상태에서 메일을 보내면 메일링 리스트 소프트웨어에서 스팸 메시지로
취급할 수도 있습니다.</p>

<ul class="roomy">

  <li>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/">구독/해지</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/">메시지 기록</a>]
    <a href="mailto:wine-announce@winehq.org">wine-announce@winehq.org</a><br>
    Wine 릴리스나 주요 Wine 및 WineHQ 뉴스를 전송하는 읽기 전용 메일링 리스트입니다.
    메시지 전송 빈도: 1개월에 2여통(낮음)
  </li>

  <li>
    [<a href="http://www.freelists.org/list/wine-zh">구독/해지</a>]
    [<a href="http://www.freelists.org/archive/wine-zh/">메시지 기록</a>]
    <a href="mailto:wine-zh@freelists.org">wine-zh@freelists.org</a><br>
    Wine 중국어 사용자 토론을 위한 공개 메일링 리스트입니다.
    메시지 전송 빈도: 1일에 10여통(낮음)
  </li>

  <li>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/">구독/해지</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/">메시지 기록</a>]
    <a href="mailto:wine-devel@winehq.org">wine-devel@winehq.org</a> <br>
    Wine 개발, WineHQ, 패치, 기타 Wine 개발자들의 관심사에 대해서 토론하는 공개 메일링 리스트입니다.
    메시지 전송 빈도: 1일에 50여통(중간)
  </li>

  <li>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/">구독/해지</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/">메시지 기록</a>]
    <a href="mailto:wine-commits@winehq.org">wine-commits@winehq.org</a><br>
    Git 트리의 커밋 정보를 게시하는 읽기 전용 리스트입니다.
    메시지 전송 빈도: 1일에 25여통(중간)
  </li>

  <li>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/">구독/해지</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/">메시지 기록</a>]
    <a href="mailto:wine-releases@winehq.org">wine-releases@winehq.org</a> <br>
    매 Wine 공식 릴리스마다 큰 diff 파일을 전송하는 읽기 전용 메일링 리스트입니다.
    메시지 전송 빈도: 1개월에 2여통(낮음)
  </li>

  <li>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/">구독/해지</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/">메시지 기록</a>]
    <a href="mailto:wine-bugs@winehq.org">wine-bugs@winehq.org</a><br>
    <a href="//bugs.winehq.org/">버그 추적 데이터베이스</a>의 활동을 알려 주는 읽기 전용 메일링 리스트입니다.
    메시지 전송 빈도: 1일에 100여통(높음)
  </li>

</ul>
