# WineHQ Website Template Engine

*ver 4.0* by Jeremy Newman <jnewman@codeweavers.com>

---

## Installation Requirements

* Apache 2.4 or higher
    * `mod_rewrite` enabled in Apache config
* PHP 7.0 or higher

## Setting Up

1. `cp include/htaccess.sample .htaccess`
2. edit `.htaccess`
    * change Rewritebase to the subdir path from the root of your website.
    iow, if you have the site as <http://www.example.com/winehq_org/>
    then `Rewritebase /winehq_org/`
    otherwise if this site is the root, leave Rewritebase /
3. `cp include/winehq.conf.sample include/winehq.conf`
4. edit `include/winehq.conf`
5. `cp include/globals.conf.sample include/globals.conf`
6. edit `include/globals.conf`

## Templates

For any path in your website, create a .template file in templates/en/
For example, <http://www.example.com/winehq_org/new_page>
 is `templates/en/new_page.template`
You can nest templates into subdirs.

The following variables can be used in templates.

`{$root}`         : relative path back to root of website
`{$base_url}`     : full URL of main website, as defined in winehq.conf
`{$self}`         : same as `$_SERVER['PHP_SELF']` in PHP
`{$request_uri}`  : same as `$_SERVER['REQUEST_URI']` in PHP

You can also add global vars to `include/globals.conf` and they will be available
as `{$config_[key]}` where `[key]` is the name you defined in globals.conf

## Translating

1. Edit `include/winehq.conf` and add your language code to @languages.
2. Create language subdir in templates.
4. Add your language strings to `templates/en/global/xml/trans/lang.xml`
5. Copy and edit path `templates/en/global/xml/trans/global.xml` to your
   langs subdir.
6. Copy individual `templates/en/*.template` to your langs subdir as you
   translate them. Only copy the files you plan on translating.
7. News item translations are in `news/<lang>`.

## Notes

* The font used in the WineHQ logo is: Ocean Sans Pro
* A SVG version of the logo is available at:
    <http://ftp.winehq.org/pub/wine/logos/wine_logo.svg>

